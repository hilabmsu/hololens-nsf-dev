﻿using UnityEngine;
using UnityEngine.UI;

public class ButtonBehavior : MonoBehaviour
{
    public GameObject rrGameObj;
    public Text rrButton;
    public Text closeRRButton;
    private bool visibleFlag = false;

    // Start is called before the first frame update
    void Start()
    {
        rrGameObj.SetActive(visibleFlag);
    }

    public void RRButtonClicked()
    {
        visibleFlag = !visibleFlag;

        if (visibleFlag)
        {
            rrButton.enabled = false;
            closeRRButton.enabled = true;
        }
        else
        {
            rrButton.enabled = true;
            closeRRButton.enabled = false;
        }

        rrGameObj.SetActive(visibleFlag);
    }
}
