﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.IO;
using UnityEngine.UI;
using System;

public class Client : MonoBehaviour
{
    //public GameObject chatContainer;
    //public GameObject messagePrefab;
    public GameObject outputText;
    public Text connectedText;
    public Text notConnectedText;

    //public GameObject graph;
    //public GameObject arrow;
    //public GameObject BarNumber;

    private bool socketReady;
    private TcpClient socket;
    private NetworkStream stream;
    private StreamWriter writer;
    private StreamReader reader;
    public string ClientName;
    private string deviceName = "UnityBase";


    public void ConnectToServer()
    {
        if (socketReady)
            return;
        string host = "192.168.56.2";
        int port = 30002;

        // create the socket
        try
        {
            socket = new TcpClient(host, port);
            stream = socket.GetStream();
            writer = new StreamWriter(stream);
            reader = new StreamReader(stream);
            socketReady = true;
            notConnectedText.enabled = false;
            connectedText.enabled = true;
            Debug.Log(" We connected on port " + port);
            Send("device_connect " + deviceName);
            //Send("device_subscribe gsr ON");
        }
        catch (Exception e)
        {
            Debug.Log("Socket Error: " + e.Message);
        }
    }

    private void Update()
    {
        if (socketReady)
        {
            if (stream.DataAvailable)
            {
                string data = reader.ReadLine();
                if (data != null)
                {
                    OnIncomingData(data);
                }
            }
        }
    }
    private void OnIncomingData(string data)
    {

        try
        {
            string[] ssize = data.Split(new char[0]);
            //connectedText.text = data;
            //outputText.GetComponentInChildren<Text>().text = data;

            //double newNum = Convert.ToDouble(ssize[2]);
            //Debug.Log(data);
            //if (newNum > 2.0)
            //{
            //    arrow.GetComponent<arrowRotation>().updateRotation(newNum);

            //    // I am the progress bar
            //    //BarNumber.GetComponent<progressBar>().Updateinfo(newNum);


            //    graph.GetComponent<windowGraph>().updateList(newNum);
            //}


        }
        catch
        {
            connectedText.text = data;
            //outputText.GetComponentInChildren<Text>().text = data;
        }

    }
    private void Send(string data)
    {
        if (!socketReady)
            return;
        writer.WriteLine(data);
        writer.Flush();
    }

    public void OnSendButton()
    {
        //string message = GameObject.Find("SendInput").GetComponent<InputField>().text;
        //speedj([0.2, 0.3, 0.1, 0.05, 0, 0], 0.5, 1)
        string message = "speedj([0, 0, 0, 0, 0, 2], 1)";
        Send(message);
    }

    public void OnSendButton2()
    {
        //string message = GameObject.Find("SendInput").GetComponent<InputField>().text;
        string message = "speedj([0.2, 0.3, 0.1, 0.05, 0, 0], 0.5, 1)";
        Send(message);
    }

    private void CloseSocket()
    {
        if (!socketReady)
            return;
        writer.Close();
        reader.Close();
        socket.Close();
        socketReady = false;
    }
    private void OnApplicationQuit()
    {
        CloseSocket();
    }
    private void OnDisable()
    {
        CloseSocket();
    }
}